//
//  NTSimpleNote.m
//  SecurityNote
//
//  Created by HTC on 14-9-22.
//  Copyright (c) 2014年 JoonSheng. All rights reserved.
//

#import "NTSimpleNote.h"
#import "NTSimpleNoteTool.h"

@implementation NTSimpleNote

//懒加载！
-(NSMutableArray *)datas
{

    if (_datas == nil)
    {
        _datas = [NSMutableArray array];
        
    }
    
    return _datas;
    
}



-(NSMutableArray *)queryWithData
{
    return [NTSimpleNoteTool queryWithSql];
}


-(void)upDateString:(NSString *)string forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [NTSimpleNoteTool upDateWithString:string forRowAtIndexPath:indexPath];

}

-(void)insertDatas:(NTSimpleNote *)addNote
{

    [NTSimpleNoteTool insertDatas:addNote];
}


-(void)deleteString:(NTSimpleNote *)deleteStr
{

    [NTSimpleNoteTool deleteString:deleteStr];

}

-(void)upDateInsert:(NTSimpleNote *)upDateInsert
{
    [NTSimpleNoteTool upDateInsert:upDateInsert];
    
}

@end
