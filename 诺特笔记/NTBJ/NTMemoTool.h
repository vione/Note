//
//  NTMemoTool.h
//  SecurityNote
//
//  Created by HTC on 14-9-30.
//  Copyright (c) 2014年 JoonSheng. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NTMemo;

@interface NTMemoTool : NSObject

+(NSMutableArray *)queryWithNote;

+(void)deleteNote:(int)ids;

+(void)insertNote:(NTMemo *)memoNote;

+(NTMemo *)queryOneNote:(int)ids;

+(void)updataNote:(NTMemo *)updataNote;


@end
