//
//  NTDiaryTool.h
//  SecurityNote
//
//  Created by HTC on 14-9-28.
//  Copyright (c) 2014年 JoonSheng. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NTDiary;

@interface NTDiaryTool : NSObject


+(NSMutableArray *)queryWithNote;

+(void)deleteNote:(int)ids;

+(void)insertNote:(NTDiary *)diaryNote;

+(NTDiary *)queryOneNote:(int)ids;

+(void)updataNote:(NTDiary *)updataNote;

@end
