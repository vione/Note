//
//  NTSimpleNoteTool.h
//  SecurityNote
//
//  Created by HTC on 14-9-22.
//  Copyright (c) 2014年 JoonSheng. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NTSimpleNote;

@interface NTSimpleNoteTool : NSObject


+ (NSMutableArray *)queryWithSql;

+ (void)upDateWithString:(NSString*)string forRowAtIndexPath:(NSIndexPath *)indexPath;

+ (void)insertDatas:(NTSimpleNote *)addNote;

+ (void)deleteString:(NTSimpleNote *)deleteStr;

+ (void)upDateInsert:(NTSimpleNote *)upDateInsert;

@end
