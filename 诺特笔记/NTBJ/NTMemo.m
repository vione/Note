//
//  NTMemo.m
//  SecurityNote
//
//  Created by HTC on 14-9-30.
//  Copyright (c) 2014年 JoonSheng. All rights reserved.
//

#import "NTMemo.h"
#import "NTMemoTool.h"

@implementation NTMemo
-(NSMutableArray *)queryWithNote
{
    return [NTMemoTool queryWithNote];
    
}

-(void)deleteNote:(int)ids
{
    [NTMemoTool deleteNote:ids];
    
}



-(void)insertNote:(NTMemo *)memoNote
{
    
    [NTMemoTool insertNote:memoNote];
}




-(NTMemo *)queryOneNote:(int)ids
{
    
    return [NTMemoTool queryOneNote:ids];
}




-(void)updataNote:(NTMemo *)updataNote
{
    [NTMemoTool updataNote:updataNote];
    
    
}

@end
