//
//  NTDiary.m
//  SecurityNote
//
//  Created by HTC on 14-9-28.
//  Copyright (c) 2014年 JoonSheng. All rights reserved.
//

#import "NTDiary.h"
#import "NTDiaryTool.h"

@implementation NTDiary


-(NSMutableArray *)queryWithNote
{
   return [NTDiaryTool queryWithNote];

}



-(void)deleteNote:(int)ids
{
    [NTDiaryTool deleteNote:ids];

}



-(void)insertNote:(NTDiary *)diaryNote
{

    [NTDiaryTool insertNote:diaryNote];
}




-(NTDiary *)queryOneNote:(int)ids
{

    return [NTDiaryTool queryOneNote:ids];
}




-(void)updataNote:(NTDiary *)updataNote
{
    [NTDiaryTool updataNote:updataNote];


}


@end
